# Weblogic Automation Library

Requires Java 8 for weblogic 12.1.3 libraries

Code based on examples from the book [Advanced Weblogic Server Automation][http://www.amazon.co.uk/Advanced-WebLogic-Server-Automation-Administration/dp/0991638611]

Original source code is available [here][http://rampant.cc/weblogic.htm]
